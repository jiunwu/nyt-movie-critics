package de.moviesmpp.data.entity

import kotlinx.serialization.Serializable

@Serializable
data class MediaEntity(
    val type: String,
    val src: String,
    val width: Int,
    val height: Int
)