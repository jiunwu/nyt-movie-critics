package de.moviesmpp.data.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MoviePicksEntity(
    val status: String,
    val copyright: String,
    @SerialName("has_more") val hasMore: Boolean,
    @SerialName("num_results") val numResults: Int,
    val results: List<MovieEntity>
)
