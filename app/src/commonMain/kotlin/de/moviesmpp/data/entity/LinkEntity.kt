package de.moviesmpp.data.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class LinkEntity(
    val type: String,
    val url: String,
    @SerialName("suggested_link_text") val suggestedLinkText: String
)