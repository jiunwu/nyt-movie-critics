package de.moviesmpp.data.entity

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class MovieEntity(
    val display_title: String?,
    val mpaa_rating: String?,
    val critics_pick: Int?,
    val headline: String?,
    @SerialName("summary_short") val summaryShort: String?,
    @SerialName("publication_date") val publicationDate: String?,
    val byline: String?,
    @SerialName("opening_date") val openingDate: String?,
    @SerialName("date_updated") val date_updated: String?,
    @SerialName("link") val link: LinkEntity,
    @SerialName("multimedia") val media: MediaEntity
)
