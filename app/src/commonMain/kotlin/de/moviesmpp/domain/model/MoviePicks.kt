package de.moviesmpp.domain.model

import de.moviesmpp.data.entity.MoviePicksEntity

data class MoviePicks(
    val status: String,
    val copyright: String,
    val hasMore: Boolean,
    val numResults: Int,
    val results: List<Movie>
)

fun MoviePicksEntity.toModel() = MoviePicks(
    status = status,
    copyright = copyright,
    hasMore = hasMore,
    numResults = numResults,
    results = results.map { it.toModel() }
)
