package de.moviesmpp.domain.model

import com.soywiz.klock.DateFormat
import com.soywiz.klock.DateTime
import com.soywiz.klock.parse
import de.moviesmpp.data.entity.LinkEntity
import de.moviesmpp.data.entity.MediaEntity
import de.moviesmpp.data.entity.MovieEntity


private val releaseDateFormat = DateFormat("yyyy-MM-dd")

data class Movie(
    val display_title: String?,
    val mpaa_rating: String?,
    val critics_pick: Int?,
    val headline: String?,
    val summaryShort: String?,
    val publicationDate: String?,
    val byline: String?,
    val openingDate: String?,
    val date_updated: String?,
    val link: LinkEntity,
    val media: MediaEntity
)

fun MovieEntity.toModel() = Movie(
    display_title = display_title,
    mpaa_rating = mpaa_rating,
    critics_pick = critics_pick,
    headline = headline,
    summaryShort = summaryShort,
    publicationDate = publicationDate,  // TODO releaseDateFormat.parse(publicationDate).local,
    byline = byline,
    openingDate = openingDate,
    date_updated = date_updated,
    link = link,
    media = media
)