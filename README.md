# Kotlin Native Multiplatform Example With New York Times Movie Critics API

This is a Kotlin [Multiplatform project](https://kotlinlang.org/docs/reference/multiplatform.html)
trying to share as much code as possible between Android and iOS, using the API from New York Times and displays a list of movies critics retrieved from it.

App ID: 56650dec-5eb2-4f7f-a5af-5b82a52ff145

In particular, this project uses a Clean Architecture approach,
sharing the complete `data` and `domain` layer, as also the `presenter` from the `presentation` layer.
Each platform currently only implements the `View` interface and adds a native UI to visualize the data from the `presenter`.
